﻿using AlgHomeWork04;
using static System.Net.Mime.MediaTypeNames;


var singleArray = new SingleArray<int>();
for (int i = 0; i < 10; i++)
{
    singleArray.Add(i+1, i);
}

var sar = singleArray.Remove(2);
Console.WriteLine(sar);


var vectorArray = new VectorArray<int>();
for (int i = 0; i < 3; i++)
{
    for (int j = 0; j < 5; j++)
    {
        vectorArray.Add(j + 1, i, j);
    }
}

var var = vectorArray.Remove(2, 2);
Console.WriteLine(var);


var factorArray = new FactorArray<int>();

for (int i = 0; i < 3; i++)
{
    for (int j = 0; j < 3; j++)
    {
        factorArray.AddElement(i, j, j + 1);
    }
}

var far = factorArray.RemoveElement(2, 2);
Console.WriteLine(far);


PriorityQueue<string> priorityQueue = new PriorityQueue<string>();

priorityQueue.Enqueue("Третья очередь", 3);
priorityQueue.Enqueue("Первая очередь", 1);
priorityQueue.Enqueue("Третья очередь", 3);
priorityQueue.Enqueue("Вторая очередь", 2);
priorityQueue.Enqueue("Третья очередь", 3);

string? next;
while((next = priorityQueue.Dequeue()) != null)
{
    Console.WriteLine(next);
}

Console.WriteLine();