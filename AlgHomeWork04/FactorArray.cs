﻿using System;

namespace AlgHomeWork04
{
    internal class FactorArray<T>
    {
        private T[][] _arrays;

        public T this[int outerIndex, int innerIndex]
        {
            get { return _arrays[outerIndex][innerIndex]; }
            set { _arrays[outerIndex][innerIndex] = value; }
        }

        public FactorArray()
        {
            _arrays = Array.Empty<T[]>();
        }

        public void AddElement(int outerIndex, int innerIndex,   T value)
        {
            if (outerIndex >= _arrays.Length)
            {
                Array.Resize(ref _arrays, outerIndex + 1);
            }

            if (_arrays[outerIndex] == null || innerIndex >= _arrays[outerIndex].Length)
            {
                T[] innerArray = new T[innerIndex + 1];
                if (_arrays[outerIndex] != null)
                {
                    _arrays[outerIndex].CopyTo(innerArray, 0);
                }
                _arrays[outerIndex] = innerArray;
            }

            _arrays[outerIndex][innerIndex] = value;
        }

        public T RemoveElement(int outerIndex, int innerIndex)
        {
            if (_arrays[outerIndex] != null || innerIndex < _arrays[outerIndex].Length)
            {
                var item = _arrays[outerIndex][innerIndex];
                T[] innerArray = new T[_arrays[outerIndex].Length - 1];
                for (int i = 0, j = 0; i < _arrays[outerIndex].Length; i++)
                {
                    if(i != innerIndex)
                    {
                        innerArray[j] = _arrays[outerIndex][i];
                        j++;
                    }
                }
                _arrays[outerIndex] = innerArray;

                return item;
            }

            return default;
        }
    }
}
