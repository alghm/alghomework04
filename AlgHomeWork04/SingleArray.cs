﻿
namespace AlgHomeWork04
{
    internal class SingleArray<T> : IArray<T>
    {
        private T[] _values;

        public SingleArray() 
        {
            _values = Array.Empty<T>();
        }

        public void Add(T item, int index)
        {
            Resize();
            _values[index] = item;
        }

        public T Remove(int index)
        {
            if (index < _values.Length)
            {
                var item = _values[index];

                var newArray = new T[_values.Length - 1];
                for (int i = 0, j = 0; i < _values.Length; i++)
                {
                    if (i != index)
                    {
                        newArray[j] = _values[i];
                        j++;
                    }
                }
                _values = newArray;

                return item;
            }

            return default;
        }

        private void Resize()
        {
            int size = _values.Length;
            T[] newValues = new T[_values.Length + 1];
            for (int i = 0; i < size; i++)
            {
                newValues[i] = _values[i];
            }
            _values = newValues;
        }
    }
}
