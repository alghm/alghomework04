﻿
namespace AlgHomeWork04
{
    internal interface IArray<T>
    {
        void Add(T item, int index); 
        T Remove(int index);
    }
}
