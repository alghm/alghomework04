﻿
using System.Data.Common;

namespace AlgHomeWork04
{
    internal class VectorArray<T>
    {
        private T[,] _values;

        public VectorArray()
        {
            _values = new T[0, 0];
        }

        public void Add(T item, int row, int column)
        {
            Resize(row, column);
            _values[row, column] = item;
        }

        public T Remove(int row, int column)
        {
            var (countRows, countColumns) = GetSize();
            
            if (row < countRows && column < countColumns)
            {
                var item = _values[row, column];
                var newArray = new T[countRows, countColumns];
                for (int r = 0, i = 0; r < countRows; r++)
                {
                    for (int с = 0, j = 0; с < countColumns; с++)
                    {
                        if (r != row || с != column)
                        {
                            newArray[i, j] = _values[r, с];
                            j++;
                        }
                    }
                    i++;
                }
                _values = newArray;

                return item;
            }

            return default;
        }

        private void Resize(int row, int column)
        {
            var (countRows, countColumns) = GetSize();
            int newRowsSize = (countRows == row + 1) ? countRows : countRows + 1;
            int newColumnsSize = (column + 1 > countColumns) ? countColumns + 1: countColumns;
            T[,] newValues = new T[newRowsSize, newColumnsSize];

            for (int i = 0; i < countRows; i++)
            {
                for (int j = 0; j < countColumns; j++)
                {
                    newValues[i, j] = _values[i, j];
                }
            }

            _values = newValues;
        }

        private (int rows, int columns) GetSize()
        {
            int countRows = _values.GetLength(0);
            int countColumns = _values.GetLength(1);

            return (countRows, countColumns);
        }
    }
}
