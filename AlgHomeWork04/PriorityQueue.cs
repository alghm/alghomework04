﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgHomeWork04
{
    public class PriorityQueue<T>
    {
        private class Node
        {
            public T Value { get; }
            public int Priority { get; }
            public Node Next { get; set; }

            public Node(T value, int priority)
            {
                Value = value;
                Priority = priority;
            }
        }

        private Node head;

        public void Enqueue(T value, int priority)
        {
            var newNode = new Node(value, priority);

            if (head == null || priority < head.Priority)
            {
                newNode.Next = head;
                head = newNode;
            }
            else
            {
                var temp = head;
                while (temp.Next != null && temp.Next.Priority <= priority)
                {
                    temp = temp.Next;
                }
                newNode.Next = temp.Next;
                temp.Next = newNode;
            }
        }

        public T Dequeue()
        {
            if (head == null)
            {
                return default;
            }

            var value = head.Value;
            head = head.Next;
            return value;
        }
    }
}
